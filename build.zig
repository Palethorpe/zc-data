const std = @import("std");

const cflags = &[_][]const u8{ "-Wall", "-Wextra" };

pub fn addCExe(
    b: *std.build.Builder,
    name: []const u8,
    files: []const []const u8,
) *std.build.LibExeObjStep {
    const exe = b.addExecutable(name, null);
    exe.linkLibC();
    exe.addIncludeDir("include");
    exe.addCSourceFiles(files, cflags);
    exe.install();

    return exe;
}

pub fn build(b: *std.build.Builder) void {
    const exes = [_]*std.build.LibExeObjStep{
        addCExe(b, "sort_tests", &.{
            "src/sort_tests.c",
            "src/radix_sort.c",
            "src/hash.c",
        }),
        addCExe(b, "map_tests", &.{
            "src/map_tests.c",
            "src/hash.c",
        }),
    };

    const target = b.standardTargetOptions(.{});
    const mode = b.standardReleaseOptions();
    for (exes) |exe| {
        exe.setTarget(target);
        exe.setBuildMode(mode);
    }

    const tests = b.addTest("src/main.zig");
    tests.linkLibC();
    tests.addIncludeDir("include");
    tests.addCSourceFiles(&.{ "src/radix_sort.c", "src/hash.c" }, cflags);

    const test_step = b.step("test", "Run tests");
    test_step.dependOn(&tests.step);
}
