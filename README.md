# Some data structures and algorithms

These are implemented in C, but can be built and tested with Zig.

`$ zig build test`

At least radix sort can be. Some pure C tests and such can be built with meson and GCC/Clang.

```
$ meson configure build_debug
$ meson compile -C build_debug
```

