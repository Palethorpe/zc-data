#include <stdio.h>

#include "bits.h"
#include "sort.h"
#include "hash.h"

int main(void)
{
	u64 rand_ints1[] = {
		0x83f7960534beb3ea, 0x10f9e1c8f066b9e2, 0x7129efd9298876e6,
		0x81abf8485fb153d6, 0x84356fda7bf8ba26, 0xeba0e2cc3b76a0ba,
		0x43674c162edc6e6c, 0xf002252df10981be, 0x39b1b59fd31d0528,
		0x379c9218c15b39f1, 0x0
	};
	const u64 rand_ints_sorted1[] = {
		0x0,
		0x10f9e1c8f066b9e2, 0x379c9218c15b39f1, 0x39b1b59fd31d0528,
		0x43674c162edc6e6c, 0x7129efd9298876e6, 0x81abf8485fb153d6,
		0x83f7960534beb3ea, 0x84356fda7bf8ba26, 0xeba0e2cc3b76a0ba,
		0xf002252df10981be
	};
	const usize rand_ints1_len = ARRAY_SIZE(rand_ints1);

	radix_sort(rand_ints1, rand_ints1_len);

	for (usize i = 0; i < rand_ints1_len; i++) {
		const u64 a = rand_ints1[i];
		const u64 b = rand_ints_sorted1[i];

		printf("[%c] %"PRIu64" == %"PRIu64"\n", a == b ? '+' : '-', a, b);

	}

	u64 hs[0x400];

	for (int i = 0; i < 0x400; i++)
		hs[i] = hash64(i, ~0ULL);

	radix_sort(hs, 0x400);

	for (int i = 1; i < 0x400; i++) {
		const u64 p = hs[i - 1];
		const u64 c = hs[i];
		if (p > c)
			printf("[-] %d: %lx > %lx\n", i, p, c);
		if (p == c)
			printf("[-] %d: %lx == %lx\n", i, p, c);
	}

	const char *const words[] = {
		"foo", "bar", "baz", "@#$%", "b", "fortunate!"
	};
	const usize words_len = ARRAY_SIZE(words);
	const usize cat_len = words_len * words_len;
	u64 hs2[cat_len];

	for (usize i = 0; i < words_len; i++) {
		for (usize j = 0; j < words_len; j++) {
			char word[24];
			const int len =
				sprintf(word, "%s%s", words[i], words[j]);
			const u64 k = hash_strn(word, len, 64);
			const usize l = i * words_len + j;

			printf("[*] %zu: hash('%s') == %lx\n", l, word, k);

			hs2[l] = k;
		}
	}

	radix_sort(hs2, cat_len);
	for (usize i = 1; i < cat_len; i++) {
		const u64 p = hs2[i - 1];
		const u64 c = hs2[i];
		if (p > c)
			printf("[-] %zu: %lx\n", i, c);
		else if (p == c)
			printf("[-] %zu: %lx\n", i, c);
		else
			printf("[+] %zu: %lx\n", i, c);
	}

}
