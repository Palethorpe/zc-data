#include "sort.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>


def_attr static inline u8 radix_n(u64 key, usize n)
{
	return (key >> (n << 3)) & 0xff;
}

void radix_sort(u64 *keys, const usize keys_len)
{
	usize col_indx[256];
	u64 *keys_buf = calloc(keys_len, sizeof(u64));
	u64 *keys_l = keys, *keys_r = keys_buf;

	const usize max_pass = sizeof(*keys);

	for (usize p = 0; p < max_pass; p++) {
		memset(col_indx, 0, sizeof(col_indx));

		for (usize i = 0; i < keys_len; i++) {
			u8 key = radix_n(keys_l[i], p);
			col_indx[key]++;
		}

		for (usize i = 0, offs = 0; i < 256; i++) {
			const usize freq = col_indx[i];
			col_indx[i] = offs;
			offs += freq;

			if (i == 255)
				assert(offs == keys_len);
		}

		for (usize i = 0; i < keys_len; i++) {
			u8 key = radix_n(keys_l[i], p);
			usize col = col_indx[key]++;
			keys_r[col] = keys_l[i];
		}

		assert(col_indx[255] == keys_len);

		keys = keys_r;
		keys_r = keys_l;
		keys_l = keys;
	}

	if (keys == keys_buf) {
		memcpy(keys_l, keys, keys_len * sizeof(*keys));
		keys = keys_l;
	}

	free(keys_buf);
}
