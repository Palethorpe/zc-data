#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "bits.h"
#include "map.h"

static inline isize hash_map_str_cmp(const u64 s1, const u64 s2)
{
	return strcmp((char *)s1, (char *)s2);
}

static inline u64 str_hash(const char *const str, usize cap)
{
	return hash_str(str, fls64(cap)) % cap;
}

static void hash_map64_print_info(const struct hash_map64 *const self)
{
	for (usize i = 0; i < self->slab_nr; i++) {
		const struct hash_slab64 *const slab = self->slabs + i;

		for (usize j = 0; j < ARRAY_SIZE(slab->slots); j++) {
			const usize k = i * 7 + j;

			printf("[*] [%zu, %zu: %zu]", i, j, k);
			switch(slab->slot.kinds[j]) {
			case hash_slot_nil:
				printf(" .\n");
				break;
			case hash_slot_imm: {
				const char *const val = (char *)slab->slots[j].imm_val;
				const u64 hash = str_hash(val, self->slot_nr);
				printf(" = '%s'\n", val);
				if (hash != k)
					printf("[-] hash of '%s' is %lu\n", val, hash);
				break;
			}
			case hash_slot_ptr: {
				const u64 *val;

				printf(" -> { ");
				for_each_slab64_u64(slab->slots[j].bucket, val) {
					const u64 hash = str_hash((char *)*val,
								  self->slot_nr);

					printf("'%s'/%ld ", (char *)*val, hash);
				}
				printf(" }\n");
				break;
			}
			default:
				zcd_assert(0, "unreachable");
			}
		}
	}
}

int main(void)
{
	const char *const words[] = {
		"z", "a", "$1234", "the", "is", "to", "from", "and",
		"end", "for", "if", "while", "case", "const", "struct",
		"enum", "hash_slot_kind", "manifold", "fluctuations",
		"grossartig", "l33t", "collision", "collisions", "final",
		"weed", "strong", "fire", "base", "mint", "pebble",
		"sing", "dance", "pentagon", "Leeroy Jenkins", "tailor",
		"tiny", "big", "small", "wire", "finger", "ginger", "_)($DFJSDFJlksdjf",
		"effigy", "sailor", "mariner", "nonce", "wheel", "bed", "manager",
		"customer", "pink", "approximation", "beetle", "wendy", "wibble",
		"butter", "zarathustra", "bone", "cammel", "digit", "radix",
		"bbbrrrr", "bbbbbrrrr", "bacon", "sugar", "wild trees", "hot ice",
		"adjectivo"
	};

	const usize words_len = ARRAY_SIZE(words);
	struct hash_map64 *const map = hash_map64_alloc(words_len);

	printf("[*] map slab_nr:  %zu, slot_nr: %zu\n", map->slab_nr, map->slot_nr);

	for (usize i = 0; i < words_len; i++) {
		const u64 hash = str_hash(words[i], map->slot_nr);

		printf("[+] store '%s' -> %lu\n", words[i], hash);
		hash_map64_store(map,
				 hash,
				 (u64)words[i],
				 hash_map_str_cmp);
	}

	hash_map64_print_info(map);

	const char *const missing_key = "fennel seeds";
	u64 key = (u64)missing_key;
	u64 missing_hash = str_hash("fennel seeds", map->slot_nr);
	enum hash_slot_kind kind =
		hash_map64_load(map,
				missing_hash,
				&key,
				hash_map_str_cmp);
	printf("[%c] Load missing key: %lu\n", "-+"[kind == hash_slot_nil], missing_hash);

	for (usize i = 0; i < words_len; i++) {
		const u64 hash = str_hash(words[i], map->slot_nr);

		key = (u64)words[i];
		kind = hash_map64_load(map,
				       hash,
				       &key,
				       hash_map_str_cmp);
		switch (kind) {
		case hash_slot_nil:
			printf("[-] missing '%s'\n", words[i]);
			break;
		case hash_slot_imm:
		case hash_slot_ptr:
			printf("[%c] Load '%lu' == '%s'\n",
			       "-+"[key == (u64)(words[i])], key, words[i]);
			break;
		}
	}

	free(map->slab_cache);
	free(map);

	return 0;
}
