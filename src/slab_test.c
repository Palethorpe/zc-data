#include <stdio.h>

#include "slab.h"

int main(void)
{
	const usize n = 63;
	struct slab_cache64 *cache = slab_cache64_alloc(n, 64);

	for (usize i = 0; i < n; i++)
	{
		for (usize j = 0; j < 7; j++)
			cache->slabs[i].u64s[j] = 7 * i + j;
	}

	for (struct slab64 *slab = slab_cache64_get(cache);
	     slab;
	     slab = slab_cache64_get(cache)) {
		printf("[+] 0x%p", (void *)slab);
		for (usize j = 0; j < 7; j++)
			printf(" %zu", j);
		printf("\n");
	}

	slab_cache64_put(cache, cache->slabs + 2);
	slab_cache64_put(cache, cache->slabs + 11);
	slab_cache64_put(cache, cache->slabs + 34);

	for (struct slab64 *slab = slab_cache64_get(cache);
	     slab;
	     slab = slab_cache64_get(cache)) {
		printf("[+] 0x%p", (void *)slab);
		for (usize j = 0; j < 7; j++)
			printf("%zu ", j);
		printf("\n");
	}

	return 0;
}
