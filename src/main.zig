const std = @import("std");

const c = @cImport({
    @cInclude("sort.h");
    @cInclude("hash.h");
});

fn lt(_: void, lhs: u64, rhs: u64) bool {
    return lhs < rhs;
}

test "order matches" {
    var prng = std.rand.SplitMix64.init(0x505e);
    var unsorted_ints: [100000]u64 = undefined;
    for (unsorted_ints) |*n| {
        n.* = prng.next();
    }
    var sorted_ints = unsorted_ints;
    var radix_sorted_ints = unsorted_ints;

    std.sort.sort(u64, &sorted_ints, {}, lt);
    _ = c.radix_sort(&radix_sorted_ints, radix_sorted_ints.len);

    for (sorted_ints) |v, i| {
        try std.testing.expect(radix_sorted_ints[i] == v);
    }
}

test "can hash" {
    var hashes = [_]u64{0} ** 0x400;

    for (hashes) |_, i| {
        hashes[i] = c.hash64(i, 0x400);
    }

    _ = c.radix_sort(&hashes, hashes.len);

    var p = hashes[0];
    var same_count: u32 = 0;
    for (hashes[1..]) |v| {
        try std.testing.expect(p <= v);
        if (p == v)
            same_count += 1;
        p = v;
    }

    try std.testing.expect(same_count < 10);
}

test "can hash string" {
    const s1 = "abc123";
    const s2 = "abc";

    try std.testing.expect(c.hash_strn(s1, s1.len) != c.hash_strn(s2, s2.len));
}

test "can hash strings" {
    const words = [_][*:0]const u8{ "foo", "bar", "baz", "$@#$", "b" };
    var hashes: [words.len * words.len]u64 = undefined;

    inline for (words) |w1, i| {
        inline for (words) |w2, j| {
            const ps: [*:0]const u8 = w1 ++ w2;
            hashes[i * words.len + j] = c.hash_str(ps);
        }
    }

    _ = c.radix_sort(&hashes, hashes.len);

    var p = hashes[0];
    var same_count: u32 = 0;
    for (hashes[1..]) |v| {
        try std.testing.expect(p <= v);
        if (p == v) {
            same_count += 1;
        }
        p = v;
    }

    try std.testing.expect(same_count < 5);
}

pub fn main() anyerror!void {
    std.log.info("", .{});
}
