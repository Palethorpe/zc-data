#include "bits.h"

#include <errno.h>

struct str
{
	u8 len_size:7;
	u8 size_of_len_p:1;
	u8 len_or_data[7];
	u8 data[];
};

isize str_len(const struct str *const self)
{
	usize len = self->len_size;

	if (!self->size_of_len_p)
		return len;

	if (self->len_size >= sizeof(isize))
		return -EDOM;

	len = 0;
	for (int r = 0; r < self->len_size; r++) {
		len *= 256;
		len += self->len_or_data[r];
	}

	return len;
}

usize str_len_nocheck(const struct str *const self)
{
	usize len = self->len_size;

	if (!self->size_of_len_p)
		return len;

	len = 0;
	for (int r = 0; r < self->len_size; r++) {
		len *= 256;
		len += self->len_or_data[r];
	}

	return len;
}

u8 str_get_nocheck(const struct str *const self, usize i)
{
	usize len = str_len_nocheck(self);

	if (i >= len)
		return 0;
}
