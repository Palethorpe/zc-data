#include "slab.h"

int main(void)
{
	const usize n = 63;
	struct slab_cache64 *cache = slab_cache64_alloc(n, 8);

	for (usize i = 0; i < n; i += 7)
	{
		u64 *const u64s = cache->slabs[i >> 3].u64s;

		printf("[*] set slabs[%zu]\n", i);
		for (usize j = 0; j < 7; j++)
			u64s[j] = 7 * i + j;
	}

	for (struct slab64 *slab = slab_cache64_get(cache);
	     slab;
	     slab = slab_cache64_get(cache)) {
		printf("[+] 0x%p ", (void *)slab);
		for (usize j = 0; j < 7; j++)
			printf("%lu ", slab->u64s[j]);
		printf("\n");
	}

	printf("[*] Put 3\n");
	slab_cache64_put(cache, cache->slabs + 2);
	slab_cache64_put(cache, cache->slabs + 3);
	slab_cache64_put(cache, cache->slabs + 4);

	for (struct slab64 *slab = slab_cache64_get(cache);
	     slab;
	     slab = slab_cache64_get(cache)) {
		printf("[+] 0x%p ", (void *)slab);
		for (usize j = 0; j < 7; j++)
			printf("%lu ", slab->u64s[j]);
		printf("\n");
	}

	free(cache);

	return 0;
}
