#ifndef ZCD_MAP_H
#define ZCD_MAP_H

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "bits.h"
#include "hash.h"
#include "slab.h"

enum hash_slot_kind {
	hash_slot_nil,
	hash_slot_imm,
	hash_slot_ptr
} attr(packed);
ASSERT_SIZE(enum hash_slot_kind, 1);

union hash_slot64 {
	u64 imm_val;
	struct slab64 *bucket;
};
ASSERT_SIZE(union hash_slot64, 8);

struct hash_slot_kinds64 {
	u8 error_bits;
	enum hash_slot_kind kinds[7];
};
ASSERT_SIZE(struct hash_slot_kinds64, 8);

struct hash_slab64 {
	struct hash_slot_kinds64 slot;
	union hash_slot64 slots[7];
};
ASSERT_SIZE(struct hash_slab64, 64);

struct hash_map64
{
	usize slab_nr;
	usize slot_nr;
	struct slab_cache64 *slab_cache;
	struct hash_slab64 slabs[];
};

typedef isize (*const hash_map64_cmp_fn)(const u64 a, const u64 b)
	attr(warn_unused_result);

attr(warn_unused_result, const)
static inline usize hash_map64_slab_nr(const usize max_items)
{
	const usize floor_pow2 = (usize)1 << (fls64(max_items) - 1);
	const usize ceil_pow2 = floor_pow2 << !!(max_items & (floor_pow2 - 1));
	const usize max_slots = ceil_pow2 + (ceil_pow2/7 + 1);

	return max_slots / 7;
}

attr(warn_unused_result, returns_nonnull)
static inline struct hash_map64 *hash_map64_alloc(const usize max_items)
{
	const usize slab_nr = hash_map64_slab_nr(max_items);
	struct hash_map64 *const map =
		malloc(sizeof(struct hash_map64) + slab_nr * sizeof(struct hash_slab64));

	zcd_assert(map, "Allocate for %zu items; slabs %zu, slots %zu",
		   max_items, slab_nr, slab_nr * 7);

	map->slab_nr = slab_nr;
	map->slot_nr = slab_nr * 7;
	map->slab_cache = slab_cache64_alloc(max_items * 2, 8);

	for (usize i = 0; i < map->slab_nr; i++)
		memset(&map->slabs[i].slot, 0, sizeof(struct hash_slot_kinds64));

	return map;
}

attr(nonnull)
static inline void hash_map64_store(struct hash_map64 *const self,
				    const usize i,
				    const u64 key,
				    hash_map64_cmp_fn cmp)
{
	const usize offset_indx = i + (i/7 + 1);
	const usize slab_indx = offset_indx >> 3;
	const usize slot_indx = (offset_indx & 0x7) - 1;
	struct hash_slab64 *const slab = self->slabs + slab_indx;
	enum hash_slot_kind *const kind = slab->slot.kinds + slot_indx;
	union hash_slot64 *const slot = slab->slots + slot_indx;

	zcd_assert(slab_indx < self->slab_nr, "%zu < %zu", slab_indx, self->slab_nr);
	zcd_assert(slot_indx < 7, "%zu < 7", offset_indx);

	switch(*kind) {
	case hash_slot_nil:
		slot->imm_val = key;
		*kind = hash_slot_imm;
		break;
	case hash_slot_imm: {
		struct slab64 *const bckt = slab_cache64_get(self->slab_cache);

		zcd_assert(bckt, "%zu", i);

		bckt->count = 2;
		bckt->u64s[1] = slot->imm_val;
		bckt->u64s[2] = key;
		slot->bucket = bckt;
		*kind = hash_slot_ptr;
		break;
	}
	case hash_slot_ptr: {
		const usize slab_len = ARRAY_SIZE(slot->bucket->u64s);
		const u64 *bckt_slot;
		struct slab64_itr_mut itr;

		for (itr = slab64_u64_mut_itr_init(slot->bucket, &bckt_slot);
		     itr.i;
		     bckt_slot = slab64_u64_mut_itr_next(&itr)) {
			if (!cmp(*bckt_slot, key))
				return;
		}

		if (itr.j == slab_len) {
			itr.slab->next = slab_cache64_get(self->slab_cache);
			itr.slab = itr.slab->next;
			itr.j = 0;
		}

		itr.slab->u64s[itr.j] = key;
		slot->bucket->count++;
	}}
}

attr(nonnull, warn_unused_result)
static inline enum hash_slot_kind hash_map64_load(const struct hash_map64 *const self,
						  const usize i,
						  u64 *const key,
						  hash_map64_cmp_fn cmp)
{
	const usize offset_indx = i + (i/7 + 1);
	const usize slab_indx = offset_indx >> 3;
	const usize slot_indx = (offset_indx & 0x7) - 1;
	const struct hash_slab64 *const slab = self->slabs + slab_indx;
	const enum hash_slot_kind *const kind = slab->slot.kinds + slot_indx;
	const union hash_slot64 *const slot = slab->slots + slot_indx;

	zcd_assert(slab_indx < self->slab_nr, "%zu < %zu", slab_indx, self->slab_nr);
	zcd_assert(slot_indx < 7, "%zu < 7", offset_indx);

	switch(*kind) {
	case hash_slot_imm: {
		if (cmp(*key, slot->imm_val))
			return hash_slot_nil;

		*key = slot->imm_val;
		return hash_slot_imm;
	}
	case hash_slot_ptr: {
		const u64 *bckt_slot;

		for_each_slab64_u64(slot->bucket, bckt_slot) {
			if (cmp(*key, *bckt_slot))
				continue;

			*key = *bckt_slot;
			return hash_slot_ptr;
		}

		return hash_slot_nil;
	}
	case hash_slot_nil:
		return hash_slot_nil;
	default:
		zcd_assert(0, "unreachable");
	}
}

#endif
