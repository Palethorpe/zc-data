#ifndef ZCD_SLAB_H
#define ZCD_SLAB_H

#include <stdlib.h>
#include <stdio.h>

#include "attrs.h"
#include "bits.h"

#define for_each_slab64_u64(slab, slot)					\
	for (struct slab64_itr itr = slab64_u64_itr_init(slab, &slot);	\
	     itr.i;							\
	     slot = slab64_u64_itr_next(&itr))

struct slab64;
struct slab64 {
	union {
		u64 count;
		u8 bytes[64 - sizeof(struct slab64 *)];
		u32 u32s[(64 - sizeof(struct slab64 *)) >> 2];
		u64 u64s[(64 - sizeof(struct slab64 *)) >> 3];
	};
	struct slab64 *next;
};
ASSERT_SIZE(struct slab64, 64);
_Static_assert(__builtin_offsetof(struct slab64, count) == 0, "count occupies first slot");

struct slab64_itr_mut {
	u64 i;
	usize j;
	struct slab64 *slab;
};

struct slab64_itr {
	u64 i;
	usize j;
	const struct slab64 *slab;
};

struct slab_cache64 {
	struct slab64 *free_list;
	struct slab64 slabs[];
};

attr(warn_unused_result, nonnull)
static inline struct slab64_itr_mut slab64_u64_mut_itr_init(struct slab64 *const slab,
							    const u64 **const slot)
{
	const struct slab64_itr_mut itr = { slab->count, 1, slab };

	if (itr.i)
		*slot = slab->u64s + itr.j;

	return itr;
}

attr(warn_unused_result, nonnull)
static inline u64 *slab64_u64_mut_itr_next(struct slab64_itr_mut *const itr)
{
	itr->i--;
	itr->j++;

	if (itr->j > 6) {
		if (!itr->slab->next)
			return NULL;

		itr->slab = itr->slab->next;
		itr->j = 0;
	}

	return itr->slab->u64s + itr->j;
}

attr(warn_unused_result, nonnull)
static inline struct slab64_itr slab64_u64_itr_init(const struct slab64 *const slab,
						    const u64 **const slot)
{
	const struct slab64_itr itr = { slab->count, 1, slab };

	if (itr.i)
		*slot = slab->u64s + itr.j;

	return itr;
}

attr(warn_unused_result, nonnull)
static inline const u64 *slab64_u64_itr_next(struct slab64_itr *const itr)
{
	itr->i--;
	itr->j++;

	if (itr->j > 6) {
		if (!itr->slab->next)
			return NULL;

		itr->slab = itr->slab->next;
		itr->j = 0;
	}

	return itr->slab->u64s + itr->j;
}

attr(warn_unused_result, returns_nonnull)
static inline struct slab_cache64 *slab_cache64_alloc(const usize slots_nr,
						      const usize slot_size)
{
	usize slabs_size = slots_nr * slot_size;
	slabs_size += (slabs_size >> 6) * sizeof(struct slab64 *);
	slabs_size = alignsz(slabs_size, 64);
	const usize slab_nr = slabs_size >> 6;
	struct slab_cache64 *const cache =
		malloc(sizeof(struct slab_cache64) + slabs_size);

	printf("%zu, %zu\n", slab_nr, slabs_size);

	if (!cache) {
		perror("malloc");
		exit(1);
	}

	cache->free_list = cache->slabs;

	for (usize i = 0; i < (slab_nr - 1); i++)
		cache->slabs[i].next = cache->slabs + i + 1;

	cache->slabs[slab_nr - 1].next = NULL;

	return cache;
}

attr(warn_unused_result, nonnull)
static inline struct slab64 *slab_cache64_get(struct slab_cache64 *const self)
{
	struct slab64 *first_free = self->free_list;

	if (!first_free)
		return NULL;

	self->free_list = first_free->next;
	first_free->next = NULL;

	return first_free;
}

attr(nonnull)
static inline void slab_cache64_put(struct slab_cache64 *const self,
				    struct slab64 *const freed)
{
	freed->next = self->free_list;
	self->free_list = freed;
}

#endif
