#ifndef ZCD_HASH_H
#define ZCD_HASH_H

#include "attrs.h"
#include "bits.h"

#include <string.h>

/**
 * Hash key `k`
 *
 * Use as follows; where `m` is the maximum hash value and is a power
 * of 2.
 *
 * ```c
 * hash64(k, m)
 * ```
 *
 * Uses multiplication method and approximates the golden
 * ratio. Constructed as follows
 *
 * ```julia
 * A = (sqrt(big"5") - 1)/2;
 * s = BigInt(floor(A*big"2"^64));
 * ```
 *
 * See Knuth and Introduction to Algorithms, Corman... Et al.
 */
attr(warn_unused_result, const)
static inline u64 hash64(const u64 k, const u64 m)
{
	const u64 s = 0x9e3779b97f4a7c15;
	const int p = 64 - fls64(m);

	return (s * k) >> p;
}

/**
 *  Hash binary string `str` of size `len`
 *
 *  Hashes blocks of 8 bytes as u64s. Combines them with Xors. The
 *  previous hash is rotated right to avoid associativity issues
 *  (Knuth).
 */
attr(nonnull, warn_unused_result, pure)
static inline u64 hash_strn(const char *str, usize len, const int p)
{
	const u64 s = 0x9e3779b97f4a7c15;
	u64 h = 0, k = 0;

	while (len >= sizeof(k)) {
		h = rot_shr(h) ^ (*(u64 *)str) * s;
		str += sizeof(k);
		len -= sizeof(k);
	}

	memcpy(&k, str, len);

	return (rot_shr(h) ^ s * k) >> (64 - p);
}


attr(warn_unused_result, pure)
static inline u64 hash_str(const char *str, const int p)
{
	return hash_strn(str, strlen(str), p);
}

#endif
