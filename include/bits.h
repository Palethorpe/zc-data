#ifndef ZCD_BITS_H
#define ZCD_BITS_H

#include <sys/types.h>
#include <inttypes.h>
#include <stdio.h>
#include <execinfo.h>

#include "attrs.h"

#define ARRAY_SIZE(a) (sizeof((a)) / sizeof((a)[0]))
#define ASSERT_SIZE(T, s) _Static_assert(sizeof(T) == s, "sizeof("#T") != "#s)

#define zcd_assert(expr, ...) do {					\
	if (expr)							\
		break;							\
									\
	fprintf(stderr, "%s:%s:%i Assert Fail '" #expr "': ",		\
		__FILE__, __func__, __LINE__);				\
	fprintf(stderr, ##__VA_ARGS__);					\
	fprintf(stderr, "\n");						\
									\
	void *buf[BUFSIZ];						\
	int i, nptrs = backtrace(buf, BUFSIZ);				\
	for (i = 0; i < nptrs; i++)					\
		fprintf(stderr, "\t%p\n", buf[i]);			\
	exit(1);							\
} while (0);


typedef int8_t  i8;
typedef uint8_t u8;
typedef int16_t  i16;
typedef uint16_t u16;
typedef int32_t  i32;
typedef uint32_t u32;
typedef int64_t  i64;
typedef uint64_t u64;
typedef ssize_t isize;
typedef size_t usize;

/**
 * Find Last Set bit
 *
 * Find the index of the most significant bit. The 64th bit being the
 * highest.
 *
 * This is adapted from the Linux kernel. Of course there are
 * intrinsics...
 */
attr(warn_unused_result, const) static inline int fls64(u64 i)
{
	int bit = 64;

	for (usize s = 32; s > 0; s >>= 1) {
		if ((i & (~(u64)0 << (64 - s))))
			continue;

		bit -= s;
		i <<= s;
	}

	return bit;
}

/** rotate right  */
static inline u64 rot_shr(u64 v)
{
	const int shl = 8 * sizeof(v) - 1;

	return (((v & 1) << shl) | v >> 1);
}

attr(warn_unused_result, const) static inline usize alignsz(const usize sz,
							    const usize align)
{

	const usize mask = align - 1;

	return (~mask & sz) + align * !!(mask & sz);
}

attr(warn_unused_result, const)
static inline usize next_pow2(const usize n)
{
	const usize floor_pow2 = (usize)1 << (fls64(n) - 1);
	const usize ceil_pow2 = floor_pow2 << !!(n & (floor_pow2 - 1));

	return ceil_pow2;
}

#endif
