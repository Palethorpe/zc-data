#ifndef ZCD_ATTRS_H
#  define ZCD_ATTRS_H

#  define attr(...) __attribute__((__VA_ARGS__))

#  define nonnull_attr attr(nonnull)
#  define warn_unused_attr attr(warn_unused_result)
#  define access_attr(...) attr(access (__VA__ARGS__))

#  define def_attr attr(nonnull, warn_unused_result)
#endif
